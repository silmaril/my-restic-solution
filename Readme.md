# My Restic Solution

This repo contains the building blocks I use to back up my Linux PC on a `rest-server` using `restic`.

This is no ready-to-use backup solution! You have to take the time to understand the scripts and change them according
to your needs.

## Client

The client scripts define a backup task that creates a BTRFS snapshot of `/home` and everything else needed to start a
restic snapshot from a chroot environment, so the path of the snapshot is still `/home` instead of some snapshot mount
point.

This was discussed in
[this forum thread](https://forum.restic.net/t/creating-btrfs-snapshot-before-runnning-restic-backup/4725).

I am using Linux Mint, so this should work identically on most Ubuntu-based distros.

### systemd

The files in the `systemd` folder contain the configuration for a timer and a service unit to start the restic backup
task automatically. It calls the script `/root/restic/restic-backup-task.sh`.

References:

- https://wiki.ubuntuusers.de/systemd/Timer_Units/,
- https://wiki.ubuntuusers.de/systemd/Service_Units/.

### bash script

The bash scripts are designed to be copied to `/root/restic`.

In this folder, there has to be a file `repo_main.pw` that contains only the password for the repo. It should have
`600` privileges (read/write for root and noone else). This is not included in this repo.

The main script called by the systemd service is `restic-backup-task.sh`. It calls all the other scripts. See comments
inside the scripts for further details.

`restic_repo_main_definitions.sh` is especially important. All paramter definitions are inside this file and are used by
all other scripts.

## Server

My restic repo is accessed in form of a rest-server instance, which is running inside a FreeBSD jail on a FreeNAS system
with `--append-only` option set. This means we get a bit of a WORM behaviour: We can send new snapshots to the server,
but we cannot execute any commands that would delete anything via this interface.

This concept was also
[discussed in the restic forum](https://forum.restic.net/t/setting-up-restic-as-a-worm-server/4710).

This is what `restic_repo_main_servertask.sh` does. This script is running on the server every night to do all the
forget / prune / check tasks.

It is started by a crontab entry that looks like this:

```crontab
# m  h  dom mon dow   command
  5  3  *   *   *     /root/restic/restic_repo_main_servertask.sh > /root/restic/last_server_task.log
  55 *  *   *   *     /root/restic/restic_repo_main_list_snapshots.sh
```

This means the script output is written to a file. In the last line of the bash script, this file is copied to a local
webserver using `scp`, so I can simply use a web browser to check it.

The script `restic_repo_main_list_snapshots.sh` is called every hour. It generates some files with information about
the current state of the repo.

I'm not sure which of the files will be the best to integrate in some automatic monitoring, so there are a few options
to chose from.
