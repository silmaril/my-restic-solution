#!/bin/bash
# Restic task that is started from the systemd service unit restic-backup-task.service
# This contains all the commands to take BTRFS snapshots and create a working chroot environment.
# The backup commands are then executed by the chroot command, which runs another script.
#
# To start the backup once, independent from the timer:
#     systemctl start restic-backup-task.service
#
# To look at the logs use the systemd functionality:
#     journalctl -u restic-backup-task.service

set -euo pipefail

echo $(realpath $(dirname "$0"))
cd "$(dirname "$0")" || exit
source "./restic_repo_main_definitions.sh"

echo "[restic] Teardown old environment before Setup..."
source "./restic-backup-task_teardown_env.sh"

source "./restic-backup-task_setup_env.sh"

echo "[restic] chroot into backup environment..."
echo $(realpath $(dirname "$0"))
chroot "$btrfs_snapshot_dir" "$(realpath $(dirname "$0"))/restic-backup-task_chroot_actions.sh"

source "./restic-backup-task_teardown_env.sh"

echo "[restic] Done."
