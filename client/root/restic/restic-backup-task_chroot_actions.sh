#!/bin/bash
# This is the chroot script, called by restic-backup-task.sh
# It assumes that it is running inside a chroot environment that has the same directory structure
# as the original system.

source "$(dirname "$0")/restic_repo_main_definitions.sh" || exit

echo "[restic-chroot] Create new snapshot from $sourcedir on repo $repo_url ..."
resticcfg unlock
resticcfg backup --verbose --one-file-system --exclude-file "$excludefile" --exclude-caches --one-file-system --tag "btrfssnap" "$sourcedir"
echo "[restic-chroot] Done."
