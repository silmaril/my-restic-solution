#!/bin/bash
# This is the part of the script that sets up everything for the next step, which is to
# chroot into $btrfs_snapshot_dir
# Precondition: Current working dir must be the directory of this script.

source "./restic_repo_main_definitions.sh"

echo "[restic] Create BTRFS snapshot for / ..."
btrfs subvolume snapshot / "$btrfs_snapshot_dir" || exit

echo "[restic] Remove /home directory from snapshot..."
rmdir "$btrfs_snapshot_dir/home" || exit
echo "[restic] Create BTRFS snapshot for /home ..."
btrfs subvolume snapshot /home "$btrfs_snapshot_dir/home" || exit

echo "[restic] Mount /proc inside chroot environment..."
mount -t proc proc "$btrfs_snapshot_dir/proc" || exit

echo "[restic] Mount /run inside chroot environment..."
mount --bind /run "$btrfs_snapshot_dir/run" || exit
