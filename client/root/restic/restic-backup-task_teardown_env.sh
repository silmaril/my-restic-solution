#!/bin/bash
# This is the part of the script that removes everything that was set up in the setup_env script.
# Precondition: Current working dir must be the directory of this script.

source "./restic_repo_main_definitions.sh"

# Unset fail on error, because we want all teardown commands to run in all cases.
set +e

echo "[restic] Unmount chroot /run ..."
umount "$btrfs_snapshot_dir/run"

echo "[restic] Unmount chroot /proc ..."
umount "$btrfs_snapshot_dir/proc"

echo "[restic] Remove BTRFS snapshot /home ..."
btrfs subvolume delete "$btrfs_snapshot_dir/home"

echo "[restic] Remove BTRFS snapshot / ..."
btrfs subvolume delete "$btrfs_snapshot_dir"

# Set fail on error again.
set -e
