#!/bin/bash
set -euo pipefail
shopt -s expand_aliases

repo_url="rest:http://my.rest-server:8000/repo_main"
pwfile="/root/restic/repo_main.pw"
excludefile="/root/restic/restic_exclude.txt"
repo_contents_mountpoint="/mnt/restic/repo_contents"
sourcedir="/home"
btrfs_snapshot_dir="/mnt/restic/btrfs-snapshot"

alias resticcfg='restic -r "$repo_url" --password-file "$pwfile"'
