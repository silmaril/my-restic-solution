# This file contains definitions used in the other scripts and is sourced in all of them.

shopt -s expand_aliases

# Directory of the restic repo
repodir="/var/db/restic-rest-db/repo_main"

# Password file should habe 600 privileges!
pwfile="/root/restic/repo_main.pw"

# Write output files to this directory
outputdir="/var/restic_out"

# Remove policy to use for the whole repo
remove_policy="--keep-last 5 --keep-hourly 8 --keep-daily 7 --keep-weekly 6 --keep-monthly 12 --keep-tag keep"

# --max-unused for prune
maxunused="50G"

# Uncomment one of these lines, depending on if you wish to do a dry-run with forget and prune.
#remove_dryrun="--dry-run"
remove_dryrun=""

# Define an alias for restic with our configuration. Use full path to restic, so we don't run into problems finding it in cron context.
alias restic_cfg='/usr/local/bin/restic -r "$repodir" --password-file "$pwfile"'
