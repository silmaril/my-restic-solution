#!/usr/local/bin/bash

# This script can be called every hour or so to update some files with information about the
# current state of the repo.
# It generates some snapshot lists in different configurations.

cd "${0%/*}"
source ./restic_repo_main_definitions.sh

# Complete list of snapshots grouped by host
restic_cfg snapshots --group-by host > "$outputdir/snapshots.txt"

# Complete list of snapshots grouped by host as JSON
restic_cfg snapshots --json --group-by host > "$outputdir/snapshots.json"

# Only the latest snapshot for each host
restic_cfg snapshots --latest 1 > "$outputdir/snapshots_latest.txt"

# Only the latest snapshot for each host as JSON
restic_cfg snapshots --latest 1 --json > "$outputdir/snapshots_latest.json"

# Only the latest snapshot for a single host as JSON
restic_cfg snapshots --latest 1 --host myhostnameA --json > "$outputdir/snapshots_latest_myhostnameA.json"
restic_cfg snapshots --latest 1 --host myhostnameB --json > "$outputdir/snapshots_latest_myhostnameB.json"

# Copy all those files to the web server
/usr/bin/scp $outputdir/* my.local.webserver:/var/www/html
