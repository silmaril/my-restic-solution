#!/usr/local/bin/bash

# This is the nightly server task that takes care of forget + prune + check.
# This script expects to be called with an output redirect like this:
# ./restic_repo_main_servertask.sh > /root/restic/last_server_task.log

cd "${0%/*}"
source ./restic_repo_main_definitions.sh

printf "########################################################################################################################\n"
printf "### Restic Server Task Start $(date +'%Y-%m-%d %H:%M:%S')\n"
printf "########################################################################################################################\n"
printf "### Restic forget for all hosts $(date +'%Y-%m-%d %H:%M:%S')\n"
restic_cfg forget $remove_policy --group-by host $remove_dryrun
printf "########################################################################################################################\n"
printf "### Restic prune repo $(date +'%Y-%m-%d %H:%M:%S')\n"
restic_cfg prune --max-unused $maxunused $remove_dryrun
printf "########################################################################################################################\n"
printf "### Restic check repo $(date +'%Y-%m-%d %H:%M:%S')\n"
restic_cfg check
printf "########################################################################################################################\n"
printf "### Make sure all repo files are owned by restserver $(date +'%Y-%m-%d %H:%M:%S')\n"
chown -R restserver:restserver /var/db/restic-rest-db/*
printf "########################################################################################################################\n"
printf "### Current size of complete repo (in SI GB) $(date +'%Y-%m-%d %H:%M:%S')\n"
du --si -d 0 /var/db/restic-rest-db/repo_main/
printf "########################################################################################################################\n"
printf "### Restic Server Task End $(date +'%Y-%m-%d %H:%M:%S')\n"
printf "########################################################################################################################\n"

# Finally copy the output we just generated to the web server, so we can easily check the results.
/usr/bin/scp /root/restic/last_server_task.log my.local.webserver:/var/www/html
